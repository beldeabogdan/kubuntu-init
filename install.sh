sudo apt update -y
sudo apt upgrade -y
sudo apt install -y wget curl tree zsh

echo "Changing default shell to zsh..."
sudo chsh -s /usr/bin/zsh
sudo chsh root -s /usr/bin/zsh

echo "Loading oh-my-zsh and Powerlevel10k config..."
cp -R ./dot_oh_my_zsh ~/.oh-my-zsh
cp ./dot_zshrc ~/.zshrc
cp ./dot_zshrc.pre-oh-my-zsh ~/.zshrc.pre-oh-my-zsh
cp ./dot_zshrc.zni ~/.zshrc.zni
cp ./dot_p10k.zsh ~/.p10k.zsh

echo "Installing Meslo fonts..."
mkdir -p /usr/local/share/fonts/m
cp fonts/* /usr/local/share/fonts/m/

echo "Loading general theme config..."
cp -R ./dot_kde ~/.kde
cp ./dot_config/katerc ~/.config/katerc
cp ./dot_config/kwinrc ~/.config/kwinrc
