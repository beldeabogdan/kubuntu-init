rm -rf dot_config
mkdir dot_config

# KDE config
rm -rf dot_kde
mkdir dot_kde
cp -R ~/.kde ./dot_kde 
cp ~/.config/katerc ./dot_config/katerc
cp ~/.config/kwinrc ./dot_config/kwinrc

# ZSH theme and config`
cp -R  ~/.oh-my-zsh ./dot_oh_my_zsh
cp ~/.zshrc ./dot_zshrc
cp ~/.zshrc.pre-oh-my-zsh ./dot_zshrc.pre-oh-my-zsh
cp ~/.zshrc.zni ./dot_zshrc.zni
cp ~/.p10k.zsh ./dot_p10k.zsh
